package com.arturorsmd.sharedpreferences;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {

    SharedPreferences genderPrefs;
    LinearLayout mainLinearLayout;
    Button buttonDelete;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mainLinearLayout = findViewById(R.id.main_linear_layout);
        buttonDelete = findViewById(R.id.button_delete);



        genderPrefs = getSharedPreferences("genderPrefs", MODE_PRIVATE);
        String gender = genderPrefs.getString("gender", "Not value");

        validateGender(gender);

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                genderPrefs.edit().clear().commit();
                Intent intent = new Intent(HomeActivity.this, SplashActivity.class);
                startActivity(intent);
                finish();

            }
        });













    }

    public void validateGender(String gender){
        if (gender.equals("boy")){

            mainLinearLayout.setBackgroundResource(R.color.blueColor);

        } else{
            mainLinearLayout.setBackgroundResource(R.color.pinkColor);
        }
    }
}
