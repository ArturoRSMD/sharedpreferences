package com.arturorsmd.sharedpreferences;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {

    private static final long SPLASH_SCREEN_DELAY = 1500;
    SharedPreferences genderPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        genderPrefs = getSharedPreferences("genderPrefs", MODE_PRIVATE);
        final String gender = genderPrefs.getString("gender", "Not value");

        //Toast.makeText(this, gender, Toast.LENGTH_SHORT).show();

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {

                if (gender.equals("boy")){
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(intent);
                } else if (gender.equals("girl")){

                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(intent);

                } else {

                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                }


            }
        };



        Timer timer = new Timer();
        timer.schedule(timerTask, SPLASH_SCREEN_DELAY);


    }
}
