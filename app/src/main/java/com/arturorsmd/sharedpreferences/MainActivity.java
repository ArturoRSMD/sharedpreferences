package com.arturorsmd.sharedpreferences;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;

public class MainActivity extends AppCompatActivity {

    Button btnSelectedBoy;
    Button btnSelectedGirl;

    SharedPreferences genderPrefs;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSelectedBoy = findViewById(R.id.btn_selected_boy);
        btnSelectedGirl = findViewById(R.id.btn_selected_girl);

        genderPrefs = getSharedPreferences("genderPrefs", MODE_PRIVATE);
        editor = genderPrefs.edit();

        btnSelectedBoy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                editor.putString("gender", "boy");
                editor.commit();

                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(intent);

            }
        });

        btnSelectedGirl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putString("gender", "girl");
                editor.commit();

                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(intent);



            }
        });
    }

    public void showMessage(int title, int description, int color){

        MaterialStyledDialog dialog = new MaterialStyledDialog.Builder(this)
                .setTitle(title)
                .setDescription(description)
                .setPositiveText(R.string.txt_confirm)
                .setHeaderColor(color)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        Toast.makeText(MainActivity.this, "Simon", Toast.LENGTH_SHORT).show();

                    }
                })
                .withDialogAnimation(true)
                .build();
        dialog.show();
    }
}
